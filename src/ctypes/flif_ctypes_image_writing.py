# -*- coding: utf-8 -*-
#
# Copyright (c) 2020 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Lesser General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--


__all__ = ["flif_image_writer"]


import typing as tp
import ctypes as ct



class flif_image_writer(object):

    create_image_RGBA     : tp.Callable[[int,int],ct.c_void_p]

    import_image_RGBA     : tp.Callable[[int,int,ct.c_void_p,int],ct.c_void_p]
    import_image_RGB      : tp.Callable[[int,int,ct.c_void_p,int],ct.c_void_p]
    import_image_GRAY     : tp.Callable[[int,int,ct.c_void_p,int],ct.c_void_p]
    import_image_GRAY16   : tp.Callable[[int,int,ct.c_void_p,int],ct.c_void_p]
    import_image_PALETTE  : tp.Callable[[int,int,ct.c_void_p,int],ct.c_void_p]

    # write_row_PALETTE8    : tp.Callable[[ct.c_void_p,int,ct.c_void_p,int],tp.NoReturn]
    # write_row_GRAY8       : tp.Callable[[ct.c_void_p,int,ct.c_void_p,int],tp.NoReturn]
    # write_row_GRAY16      : tp.Callable[[ct.c_void_p,int,ct.c_void_p,int],tp.NoReturn]
    # write_row_RGBA8       : tp.Callable[[ct.c_void_p,int,ct.c_void_p,int],tp.NoReturn]
    # write_row_RGBA16      : tp.Callable[[ct.c_void_p,int,ct.c_void_p,int],tp.NoReturn]

    set_palette           : tp.Callable[[ct.c_void_p,ct.c_void_p,int],tp.NoReturn]
    set_frame_delay       : tp.Callable[[ct.c_void_p,int],tp.NoReturn]

    destroy_image         : tp.Callable[[ct.c_void_p],tp.NoReturn]


    @classmethod
    def initialize(cls, fliflib ):

        # FLIF_IMAGE* flif_create_image(uint32_t width, uint32_t height);
        cls.create_image = fliflib.flif_create_image
        cls.create_image.argtypes = [ ct.c_uint32, ct.c_uint32 ]
        cls.create_image.restype  = ct.c_void_p

        # FLIF_IMAGE* flif_import_image_RGBA(uint32_t width, uint32_t height, const void* rgba, uint32_t rgba_stride);
        cls.import_image_RGBA = fliflib.flif_import_image_RGBA
        cls.import_image_RGBA.argtypes = [ ct.c_uint32, ct.c_uint32, ct.c_void_p, ct.c_uint32 ]
        cls.import_image_RGBA.restype = ct.c_void_p

        # FLIF_IMAGE* flif_import_image_RGB(uint32_t width, uint32_t height, const void* rgb, uint32_t rgb_stride);
        cls.import_image_RGB = fliflib.flif_import_image_RGB
        cls.import_image_RGB.argtypes = cls.import_image_RGBA.argtypes
        cls.import_image_RGB.restype  = cls.import_image_RGBA.restype
        
        # FLIF_IMAGE* flif_import_image_GRAY(uint32_t width, uint32_t height, const void* gray, uint32_t gray_stride);
        cls.import_image_GRAY = fliflib.flif_import_image_GRAY
        cls.import_image_GRAY.argtypes = cls.import_image_RGBA.argtypes
        cls.import_image_GRAY.restype  = cls.import_image_RGBA.restype
        
        # FLIF_IMAGE* flif_import_image_GRAY16(uint32_t width, uint32_t height, const void* gray, uint32_t gray_stride);
        cls.import_image_GRAY16 = fliflib.flif_import_image_GRAY16
        cls.import_image_GRAY16.argtypes = cls.import_image_RGBA.argtypes
        cls.import_image_GRAY16.restype  = cls.import_image_RGBA.restype
        
        # FLIF_IMAGE* flif_import_image_PALETTE(uint32_t width, uint32_t height, const void* gray, uint32_t gray_stride);
        cls.import_image_PALETTE = fliflib.flif_import_image_PALETTE
        cls.import_image_PALETTE.argtypes = cls.import_image_RGBA.argtypes
        cls.import_image_PALETTE.restype  = cls.import_image_RGBA.restype

        # # void flif_image_write_row_PALETTE8(FLIF_IMAGE* image, uint32_t row, const void* buffer, size_t buffer_size_bytes);
        # cls.write_row_PALETTE8 = fliflib.flif_image_write_row_PALETTE8
        # cls.write_row_PALETTE8.argtypes = [ ct.c_void_p, ct.c_uint32, ct.c_void_p, ct.c_size_t ]

        # # void flif_image_write_row_GRAY8(FLIF_IMAGE* image, uint32_t row, const void* buffer, size_t buffer_size_bytes);
        # cls.write_row_GRAY8 = fliflib.flif_image_write_row_GRAY8
        # cls.write_row_GRAY8.argtypes = cls.write_row_PALETTE8.argtypes

        # # void flif_image_write_row_GRAY16(FLIF_IMAGE* image, uint32_t row, const void* buffer, size_t buffer_size_bytes);
        # cls.write_row_GRAY16 = fliflib.flif_image_write_row_GRAY16
        # cls.write_row_GRAY16.argtypes = cls.write_row_PALETTE8.argtypes

        # # void flif_image_write_row_RGBA8(FLIF_IMAGE* image, uint32_t row, const void* buffer, size_t buffer_size_bytes);
        # cls.write_row_RGBA8 = fliflib.flif_image_write_row_RGBA8
        # cls.write_row_RGBA8.argtypes = cls.write_row_PALETTE8.argtypes

        # # void flif_image_write_row_RGBA16(FLIF_IMAGE* image, uint32_t row, const void* buffer, size_t buffer_size_bytes);
        # cls.write_row_RGBA16 = fliflib.flif_image_write_row_RGBA16
        # cls.write_row_RGBA16.argtypes = cls.write_row_PALETTE8.argtypes

        # void flif_image_set_palette(FLIF_IMAGE* image, const void* buffer, uint32_t palette_size)
        cls.set_palette = fliflib.flif_image_set_palette
        cls.set_palette.argtypes = [ ct.c_void_p, ct.c_void_p, ct.c_uint32 ]

        # void flif_image_set_frame_delay(FLIF_IMAGE* image, uint32_t delay);
        cls.set_frame_delay = fliflib.flif_image_set_frame_delay
        cls.set_frame_delay.argtypes = [ ct.c_void_p, ct.c_uint32  ]

        # void flif_destroy_image(FLIF_IMAGE* image);
        cls.destroy_image = fliflib.flif_destroy_image
        cls.destroy_image.argtypes = [ ct.c_void_p ]

