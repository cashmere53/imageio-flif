# -*- coding: utf-8 -*-
#
# Copyright (c) 2020 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Lesser General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--


__all__ = ["FlifReaderBase"]

import typing as tp
import ctypes as ct
import numpy as np

from .ctypes import flif_decoder
from .flif_image import FlifImage


class FlifReaderBase( object ):

    __decoder_handle : tp.Optional[ct.c_void_p]

    preserve_quantization : bool


    def __init__(self):
        #super().__(*args, **argv)
        self.__decoder_handle = None


    def _open(self, preserve_quantization : bool = False ):
        """
        Parameters for reading
        ----------------------
        preserve_quantization : (Optional) bool
            Only relevant for images with quantization palette.
            The quantization palette (if relevant) becomes part of image 
            meta data dictionary: img.meta["palette"]
            Parameter values:
            True: preserve quantization indices in output image [WxH].
            False: (default) map quantization indices back into color space.
                   The image becomes shape [WxHx4]
        """

        self.preserve_quantization = bool(preserve_quantization)

        # create decoder
        decoder_handle = flif_decoder.create_decoder()
        if not bool(decoder_handle):
            raise MemoryError("Error creating FLIF decoder")
        self.__decoder_handle = decoder_handle
    
        # decode file
        #ifile = self.request.get_file()
        filename = self.request.get_local_filename()
        if 0 == flif_decoder.decode_file( self.__decoder_handle, filename.encode('utf-8') ):
            raise IOError("Error decoding FLIF file %r" % filename )
    

    def _close(self):
        if  self.__decoder_handle is not None:
            flif_decoder.abort_decoder( self.__decoder_handle )
            flif_decoder.destroy_decoder( self.__decoder_handle )
            self.__decoder_handle = None


    def _get_length(self) -> int:
        return flif_decoder.num_images( self.__decoder_handle )


    def _get_data(self, index):
        # Return the data and meta data for the given index
        if index >= self._get_length():
            raise IndexError("Image index %i >= %i" % (index, self._get_length()))
        
        image_handle = flif_decoder.get_image_handle( self.__decoder_handle, int(index) )
        
        if not bool(image_handle):
            raise IOError("Error reading image %d" % index)

        flif_image = FlifImage( image_handle, take_ownership=False )
        npy_image = flif_image.get_npy_image()
        meta_info = { 'duration' : flif_image.frame_delay }

        # get palette if requested anf if available
        if flif_image.palette_size > 0:
            assert np.issubdtype( npy_image.dtype, np.uint8 )
            palette = flif_image.get_palette()
            meta_info["palette"] = palette

            if not self.preserve_quantization:
                # map to real colors
                npy_image = palette.take( npy_image, axis=0 )

        return (npy_image, meta_info)


    def _get_meta_data(self, index):
        # Get the meta data for the given index. If index is None, it
        # should return the global meta data.
        return {}  # This format does not support meta data

